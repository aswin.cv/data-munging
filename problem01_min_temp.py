import csv, re
from read_file import get_data
from remove_special_caharacters import get_removed_whitespace_line

def get_min_temp(data):
    days_temp={}
    min_temp=0
    min_temp_day = 0
    for each_row in data:
        if len(each_row) > 0:
            replaced_data=get_removed_whitespace_line(str(each_row[0]))
            try:
                days_temp[replaced_data[1]] = int(replaced_data[3])
            except Exception :
                min_temp=replaced_data[3]
                min_temp_day=replaced_data[1]
                break
    return [min_temp_day,min_temp]





if __name__ == "__main__":
    data=get_data("./data/weather.dat")
    min_temperature = get_min_temp(data[0])
    print(f'Lowest Temperature {min_temperature[1]} at day {min_temperature[0]}')
    data[1].close()