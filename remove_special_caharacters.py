import re

def get_removed_whitespace_line(line):
    regex=r'[\s\.-]{1,}'
    return re.sub(regex,',', line).split(",")