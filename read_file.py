import csv

def get_data(file_name):
    file = open(file_name)
    next(file, None)
    raw_data=csv.reader(file)
    return [raw_data,file]

