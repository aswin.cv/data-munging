import csv,re,sys,math
from read_file import get_data
from remove_special_caharacters import get_removed_whitespace_line


def get_min_diff(data):
    min_diff = {}
    for each_row in data:
        replaced_data=get_removed_whitespace_line(str(each_row[0]))
        try:
            goals_for = int(replaced_data[len(replaced_data)-3])
            goals_against = int(replaced_data[len(replaced_data)-2])
            team_name=replaced_data[2]
            min_diff[team_name]= abs(goals_for-goals_against)
        except Exception:
            print()
    min_diff = sorted(min_diff.items(), key=lambda kv: kv[1])
    return min_diff


if __name__ == "__main__":
    data=get_data("./data/football.dat")
    min_goals_diff = get_min_diff(data[0])
    print(f'Min Difference {min_goals_diff[0]} ')
    data[1].close()